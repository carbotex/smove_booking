RSpec.describe Booking::Optimizer do
  describe "#minimum_relocation" do
    subject { Booking::Optimizer.new(json_string) }

    context 'sample #1' do
      let(:json_string) do
        [
          {id: 1, start: 23, end: 42},
          {id: 2, start: 77, end: 45},
          {id: 3, start: 42, end: 77 }
      ].to_json
      end

      it 'returns 0 relocations' do
        total_relocation, routes = subject.minimum_relocation

        expect(total_relocation).to eq 0
        expect(routes).to eq [1, 3, 2]
      end
    end

    context 'sample #2' do
      let(:json_string) do
        [
          {id: 1, start: 27, end: 21},
          {id: 2, start: 45, end: 27},
          {id: 4, start: 27, end: 45},
          {id: 5, start: 45, end: 27},
          {id: 3, start: 28, end: 45}
        ].to_json
      end

      it 'returns 0 relocations' do
        total_relocation, routes = subject.minimum_relocation

        expect(total_relocation).to eq 0
        expect(routes).to eq [3, 2, 4, 5, 1]
      end
    end

    context 'sample #3' do
      let(:json_string) do
        [
          { id: 1, start: 3, end: 1 },
          { id: 2, start: 1, end: 3 },
          { id: 3, start: 3, end: 1 },
          { id: 4, start: 2, end: 2 },
          { id: 5, start: 3, end: 1 },
          { id: 6, start: 2, end: 3 },
          { id: 7, start: 1, end: 3 },
          { id: 8, start: 1, end: 1 },
          { id: 9, start: 3, end: 3 },
          { id: 10, start: 3, end: 2 },
          { id: 11, start: 3, end: 3 },
          { id: 12, start: 3, end: 2 },
          { id: 13, start: 1, end: 1 },
          { id: 14, start: 1, end: 3 },
          { id: 15, start: 3, end: 2 },
          { id: 16, start: 3, end: 2 },
          { id: 17, start: 1, end: 3 },
          { id: 18, start: 3, end: 3 }
        ].to_json
      end

      it 'returns 2 relocations' do
        total_relocation, routes = subject.minimum_relocation

        expect(total_relocation).to eq 2
        expect(routes).to eq [8, 13, 2, 9, 11, 18, 1, 7, 3, 14, 5, 17, 10, 4, 6, 12, 15, 16]
      end
    end

    context 'sample #4' do
      let(:json_string) do
        [
          { id: 1, start: 3, end: 1 },
          { id: 2, start: 2, end: 4 },
          { id: 3, start: 1, end: 5 }
        ].to_json
      end

      it 'returns 1 relocation' do
        total_relocation, routes = subject.minimum_relocation

        expect(total_relocation).to eq 1
        expect(routes).to eq [1, 3, 2]
      end
    end

    context 'sample #5 - when no connection between vertices' do
      let(:json_string) do
        [
          { id: 1, start: 3, end: 6 },
          { id: 2, start: 2, end: 4 },
          { id: 3, start: 1, end: 5 }
        ].to_json
      end

      it 'returns 2 relocations' do
        total_relocation, routes = subject.minimum_relocation

        expect(total_relocation).to eq 2
        expect(routes).to eq [1, 2, 3]
      end
    end
  end
end
