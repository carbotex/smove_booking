# Booking

You are given a set of data representing a sequence of bookings on a single car. Each booking consists of a start location and an end location. Given two adjacent bookings *b1* and *b2*, we say a *relocation* is required between those bookings if the end location of *b1* ≠ the start location of *b2*.

For example,

```
using the format [start_location     end_location],

   b1        b2
[a     b] [c     d] = relocation required (b ≠ c)

   b1        b2
[a     b] [b     c] = no relocation required (b = b)
```


Your task is to design and implement an algorithm that takes a sequence of bookings as input, and outputs a _single_ permutation of the input that minimises the total number of relocations within the sequence. For example, given

```
   b1         b2        b3        b4
[x     y] [z     x] [y     z] [y     z] = 3 relocations required
```

you could return

```
   b3         b2        b1        b4
[y     z] [z     x] [x     y] [y     z] = 0 relocations required
```

If you find multiple sequences with the same number of relocations, you can return any of them - it does not matter which. It is the 'score', i.e. the number of relocations, that matters.

The output sequence must contain the same set of bookings as the input (that is, you can't add or remove any bookings), and you cannot change the start or end locations of any bookings. Note that it might not be possible to find a reordered sequence with 0 relocations. Your job is simply to minimise the number as much as possible.

Feel free to use any language/framework/library you'd like, but make sure it's easy for us to build and run!

### Input Format
A JSON file consisting of an array of booking objects. Each object has an `id`, `start`, and `end`. E.g.:

```
[
	{ "id": 1, "start": 23, "end": 42 },

	{ "id": 2, "start": 77, "end": 45},

	{ "id": 3, "start": 42, "end": 77 },

	. . .
]
```

### Output Format
A JSON file consisting of an array of booking IDs of the reordered sequence. E.g., given the above input:

```
[1, 3, 2]
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'booking'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install booking

## Usage

Pass `json_string` as parameter when initializing `Booking::Optimizer` then call `.minimum_reloaction` to get
`total_relocation` found and the `routes` representing the result.

```
  json_string = [
    {id: 1, start: 23, end: 42},
    {id: 2, start: 77, end: 45},
    {id: 3, start: 42, end: 77 }
  ].to_json

  total_relocation, routes = Booking::Optimizer.new(json_string).minimum_relocation
```

## Explanation

I identified this as graph problem with same weight (by ignoring time of booking and distance between vertices),

I tried using all permutation method (brute force), however it is too slow when trying out the sample json with 18! permutation. The
solution was O(n!), the complexity increase exponentially.

The approach is using dynamic programming by studying how human will solve the problem without using brute force.
Below is the list of steps I used to come up with the solution.

* `start_hash`: Build hash of starting point - I need to make sure all element in
  this hash are consume before the program stop.
* `same_hash`: Build hash of vertices with same start and end point.
* `possible_solutions`: Find out how many unique starting point, find different routes for each
  starting point.
* When finding possible solution, the goal is to complete all vertices
  in `same_hash` as soon as possible, then traverse through the remaining
  items in the has which have connection. When exhausted, simply append
  remaining vertices into the `routes`
* Sort possible solution by least number of relocation

This solution may not be 100% accurate, but given the fast performance, it is accurate enough to minimize the number of relocation.

The solution can be improved with some minor recursion added, i.e. every time connection stop, check on the remaining starting points in the hash and execute `traverse_path_from_starting_point` recursively.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
