module Booking
  class Optimizer
    def initialize(json_string)
      begin
        @bookings = JSON.parse(json_string)
      rescue
        raise Booking::Error.new('Error in parsing json string.')
      end
    end

    def minimum_relocation
      @start_hash = {}
      @same_hash = {}

      @bookings.each do |booking|
        if booking['start'] == booking['end']
          @same_hash[booking['start']] ||= []
          @same_hash[booking['start']] << booking
        else
          @start_hash[booking['start']] ||= []
          @start_hash[booking['start']] << booking
        end
      end

      possible_solutions = []
      @start_hash.keys.each do |key|
        possible_solutions << traverse_path_from_starting_point(key)
      end

      possible_solutions.sort_by { |k| k[0] }.first
    end

    private

    def traverse_path_from_starting_point(start)
      cloned_start_hash = deep_copy(@start_hash)
      cloned_same_hash = deep_copy(@same_hash)

      routes = []
      relocation = 0

      current = start
      while !cloned_start_hash.empty?
        if cloned_same_hash[current]
          cloned_same_hash[current].each do |route|
            routes << route['id']
          end
          cloned_same_hash.delete(current)
        end

        if cloned_start_hash[current].nil?
          relocation += 1
          current = cloned_start_hash.keys.first
        end

        next_booking = find_best_next_booking(cloned_start_hash, current)
        break if next_booking.nil?

        routes << next_booking['id']
        cloned_start_hash.delete(current) if cloned_start_hash[current].empty?

        current = next_booking['end']
      end

      [relocation, routes]
    end

    def find_best_next_booking(input_hash, current)
      input_hash[current].each_with_index do |item, index|
        unless input_hash[item['end']].nil?
          return input_hash[current].delete_at(index)
        end
      end

      return input_hash[current].shift
    end

    def deep_copy(o)
      Marshal.load(Marshal.dump(o))
    end
  end
end
