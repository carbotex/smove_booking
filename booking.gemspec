lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "booking/version"

Gem::Specification.new do |spec|
  spec.name          = "booking"
  spec.version       = Booking::VERSION
  spec.authors       = ["William Afendy"]
  spec.email         = ["william.afendy@gmail.com"]

  spec.summary       = "Booking Ordering Problem"
  spec.description   = <<-DESC
    Take a sequence of bookings as input,
    and outputs a single permutation of the input
    that minimises the total number of relocations within the sequence
  DESC

  spec.homepage      = "https://www.smove.sg/"
  spec.license       = "MIT"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "json", "~> 2.2"

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "byebug", "~> 11.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
